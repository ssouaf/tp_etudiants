<?php 
  class Poneys {
      private $count;

      public function getCount() {
        return $this->count;
      }

      public function removePoneyFromField($number) {
        if($number < 0){
          throw new Exception("You can not have a negative number", 1);      
        }
        $this->count -= $number;
      }

      public function addPoneyToField($number){
        $this->count += $number;
      }

      public function getNames() {

      }

      public function availablePlaces(){
        if($this->count>15){
          return FALSE;
        }
        return TRUE;
      }

      public function setCount($c){
        $this->count = $c;
      }

  }
?>
