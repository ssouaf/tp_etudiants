<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

    private $Poneys;
    public function setUp(){
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(8);
    }

    public function tearDown(){
      unset($this->Poneys);
    }

    public function test_removePoneyFromField() {
      // Setup
      //$Poneys = new Poneys();

      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());


    }
    /**
      * @dataProvider removeDataProvider
      */
    public function test_removePoneyFromFieldWithDP($num,$result) {
      // Setup
      //$Poneys = new Poneys();
      // Action
      $this->Poneys->removePoneyFromField($num);     
      // Assert
      $this->assertEquals($result, $this->Poneys->getCount());
    }

    public function removeDataProvider(){
      return array(
            array(-3,5),
            array(6,2),
            array(1,7),
        );
    }

    public function test_addPoneyToField() {
      //$Poneys = new Poneys();

      $this->Poneys->addPoneyToField(5);

      $this->assertEquals(13, $this->Poneys->getCount());
    }

    public function test_getNames(){

      $Poneys = new Poneys();

      $namesmock = $this->getMockBuilder(Poneys::class)
                        ->setMethods(['getNames'])
                        ->getMock();
      $namesmock->method('getNames')->willReturn(array("Poney1","Poney2"));
      
      $this->assertEquals(array("Poney1","Poney2"),$namesmock->getNames());

    }

    public function test_AvailablePlaces(){
      //$Poneys = new Poneys();

      $this->assertTrue($this->Poneys->availablePlaces());
    }
  }
 ?>
