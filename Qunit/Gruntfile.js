'use strict';

module.exports = function(grunt) {

	grunt.initConfig({

     	uglify: {
    		my_target: {
 
        	 	src: 'js/*.js',
        	  	dest: 'dest/functions.min.js'
   
    		}
 		 },
 		 
 		watch: {
			//observer les scripts
			scripts: {
			//liste des fichiers à observer
				files: ['js/*.js'],
			// tâche à exécuter au changement
				tasks: ['uglify'],
				options:{
					livereload: true
				}
			},
			//observer les styles
			//styles: {
			//files: 'css/**/*.css', // tous les fichiers css de n'importe quel dossier
			//tasks: ['cssmin']
			//},
			},
		qunit: {
     		 all: ['html/*.html']
    	}
			
   
		});
		


	// Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  // Default task(s).
  
  
  grunt.registerTask('default', ['uglify','watch','qunit']);
};