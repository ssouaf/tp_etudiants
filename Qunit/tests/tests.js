QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});


QUnit.test('estPair()', function( assert ) { 
	assert.expect(7);
    assert.ok(estPair(0), '0 est un nombre pair'); 
    assert.ok(estPair(2), '2 est un nombre pair'); 
    assert.ok(estPair(-4), '-4 est un nombre pair'); 
    assert.ok(!estPair(1), '1 n est pas un nombre pair'); 
    assert.ok(estPair(800), '800 est pas un nombre pair');
    assert.notOk(estPair(7), '7 n est pas un nombre pair');    
    assert.notOk(estPair(9), '9 n est pas un nombre pair');
});

QUnit.test('sayHello',function( assert ){
	assert.equal(sayHello('Salwa'),'Bonjour Salwa','It works');
	assert.equal(sayHello('Jalal'),'Bonjour Jalal','It works');
});

