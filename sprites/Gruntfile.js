'use strict';

module.exports = function(grunt) {

	grunt.initConfig({
		sprite:{
      		all: {
        		src: 'img/*.png',
        		dest: 'img/res/spritesheet.png',
        		destCss: 'css/sprites.css'
      		}
   		}
});
		


	// Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-spritesmith');

  // Default task(s).
  
  
  grunt.registerTask('default', ['sprite']);
};